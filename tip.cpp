#include <iostream>
using namespace std;

//Mashal Tareen, M00740589

int main()
{
  float price, total_price, tip, percentageTip;

  cout << "Enter the price(Pounds): ";
  cin >> price;
  cout << "Enter the tip percentage: ";
  cin >> percentageTip;

  //here the percentage tip is calculated
  tip = (percentageTip/100) * price;

  //here the amount calculated above is added to the total price
  total_price = price + tip;

  
  //prints the tip and the total amount
  cout << "The tip is $" << tip;
  cout << "\n The total amount to pay is : $" << total_price ;

  return 0;
}
